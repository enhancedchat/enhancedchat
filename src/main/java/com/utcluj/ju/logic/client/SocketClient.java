package com.utcluj.ju.logic.client;

import com.utcluj.ju.gui.ChatGUI;
import com.utcluj.ju.logic.model.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Bogdan on 05.11.2015.
 */
public class SocketClient implements Runnable {

  public int port;
  public String serverAddr;
  public Socket socket;
  public ChatGUI ui;
  public ObjectInputStream In;
  public ObjectOutputStream Out;

  public SocketClient(ChatGUI frame) throws IOException {
    ui = frame;
    this.serverAddr = ui.serverAddr;
    this.port = ui.port;
    socket = new Socket(InetAddress.getByName(serverAddr), port);

    Out = new ObjectOutputStream(socket.getOutputStream());
    Out.flush();
    In = new ObjectInputStream(socket.getInputStream());
  }

  public void run() {
    boolean keepRunning = true;
    while (keepRunning) {
      try {
        Message msg = (Message) In.readObject();
        System.out.println("Incoming : " + msg.toString());

        if (msg.type.equals("message")) {
          if (msg.recipient.equals(ui.username)) {
            ui.jTextArea1.append(msg.sender + " : " + msg.content + "\n");
          } else {
            ui.jTextArea1.append("Me : " + msg.content + "\n"); //TODO: deal with it
          }
        } else if (msg.type.equals("login")) {
          if (msg.content.equals("TRUE")) {
            ui.jButton1.setEnabled(false);
            ui.jButton2.setEnabled(true);
            ui.jTextArea1.append("SERVER : Login Successful\n");
            ui.jTextField1.setEnabled(false);
          } else {
            ui.jTextArea1.append("SERVER : Login Failed\n");
          }
        } else if (msg.type.equals("test")) {
          ui.jButton1.setEnabled(true);
          ui.jButton1.setEnabled(true);
          ui.jTextField1.setEnabled(true);
        } else if (msg.type.equals("newuser")) {
          if (!msg.content.equals(ui.username)) {
            boolean exists = false;
            for (int i = 0; i < ui.model.getSize(); i++) {
              if (ui.model.getElementAt(i).equals(msg.content)) {
                exists = true;
                break;
              }
            }
            if (!exists) {
              ui.model.addElement(msg.content);
            }
          }
        } else if (msg.type.equals("signout")) {
          if (msg.content.equals(ui.username)) {
            ui.jTextArea1.append(msg.sender + " : " + "Bye\n");
            ui.jButton1.setEnabled(true);
            ui.jButton2.setEnabled(false);
            ui.jTextField1.setEditable(true);
            ui.jTextField2.setEditable(true);

            for (int i = 1; i < ui.model.size(); i++) {
              ui.model.removeElementAt(i);
            }

            ui.clientThread.stop();
          } else {
            ui.model.removeElement(msg.content);
            ui.jTextArea1.append(msg.sender + " : " + msg.content + " has signed out\n");
          }
        }
      } catch (Exception ex) {
        keepRunning = false;
        ui.jTextArea1.append("Application : Connection Failure\n");

        for (int i = 1; i < ui.model.size(); i++) {
          ui.model.removeElementAt(i);
        }

        ui.clientThread.stop();

        System.out.println("Exception SocketClient run()");
        ex.printStackTrace();
      }
    }
  }

  public void send(Message msg) {
    try {
      Out.writeObject(msg);
      Out.flush();
      System.out.println("Outgoing : " + msg.toString());
    } catch (IOException ex) {
      System.out.println("Exception SocketClient send()");
    }
  }
}
