package com.utcluj.ju.logic.server;

import com.utcluj.ju.gui.ServerGUI;
import com.utcluj.ju.logic.model.Message;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Bogdan on 05.11.2015.
 */
public class SocketServer implements Runnable {

  public ServerThread clients[];
  public ServerSocket serverSocket = null;
  public Thread thread = null;
  public int clientCount = 0, port = 13000;

  public SocketServer() {
    clients = new ServerThread[50];

    try {
      serverSocket = new ServerSocket(port);
      port = serverSocket.getLocalPort();
      start();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public SocketServer(int Port) {
    clients = new ServerThread[50];
    port = Port;

    try {
      serverSocket = new ServerSocket(port);
      port = serverSocket.getLocalPort();
      start();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void run() {
    while (thread != null) {
      try {
        addThread(serverSocket.accept());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public void start() {
    if (thread == null) {
      thread = new Thread(this);
      thread.start();
    }
  }

  public void stop() {
    if (thread != null) {
      thread.stop();
      thread = null;
    }
  }

  private int findClient(int ID) {
    for (int i = 0; i < clientCount; i++) {
      if (clients[i].getID() == ID) {
        return i;
      }
    }
    return -1;
  }

  public synchronized void handle(int ID, Message msg) {
    if (msg.content.equals(".bye")) {
      Announce("signout", "SERVER", msg.sender);
      remove(ID);
    } else {
      if (msg.type.equals("login")) {
        if (findUserThread(msg.sender) == null) {
          clients[findClient(ID)].username = msg.sender;
          clients[findClient(ID)].send(new Message("login", "SERVER", "TRUE", msg.sender));
          Announce("newuser", "SERVER", msg.sender);
          SendUserList(msg.sender);
        } else {
          clients[findClient(ID)].send(new Message("login", "SERVER", "FALSE", msg.sender));
        }
      } else if (msg.type.equals("message")) {
        if (msg.recipient.equals("All")) {
          Announce("message", msg.sender, msg.content);
        } else {
          findUserThread(msg.recipient).send(new Message(msg.type, msg.sender, msg.content, msg.recipient));
          clients[findClient(ID)].send(new Message(msg.type, msg.sender, msg.content, msg.recipient));
        }
      } else if (msg.type.equals("test")) {
        clients[findClient(ID)].send(new Message("test", "SERVER", "OK", msg.sender));
      } else if (msg.type.equals("signup")) {
        if (findUserThread(msg.sender) == null) {
          clients[findClient(ID)].username = msg.sender;
          clients[findClient(ID)].send(new Message("signup", "SERVER", "TRUE", msg.sender));
          clients[findClient(ID)].send(new Message("login", "SERVER", "TRUE", msg.sender));
          Announce("newuser", "SERVER", msg.sender);
          SendUserList(msg.sender);
        } else {
          clients[findClient(ID)].send(new Message("signup", "SERVER", "FALSE", msg.sender));
        }
      }
    }

  }

  public void Announce(String type, String sender, String content) {
    Message msg = new Message(type, sender, content, "All");
    for (int i = 0; i < clientCount; i++) {
      clients[i].send(msg);
    }
  }

  public void SendUserList(String toWhom) {
    for (int i = 0; i < clientCount; i++) {
      findUserThread(toWhom).send(new Message("newuser", "SERVER", clients[i].username, toWhom));
    }
  }

  public ServerThread findUserThread(String usr) {
    for (int i = 0; i < clientCount; i++) {
      if (clients[i].username.equals(usr)) {
        return clients[i];
      }
    }
    return null;
  }

  public synchronized void remove(int ID) {
    int pos = findClient(ID);
    if (pos >= 0) {
      ServerThread toTerminate = clients[pos];
      if (pos < clientCount - 1) {
        for (int i = pos + 1; i < clientCount; i++) {
          clients[i - 1] = clients[i];
        }
      }
      clientCount--;
      try {
        toTerminate.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
      toTerminate.stop();
    }
  }

  private void addThread(Socket socket) {
    if (clientCount < clients.length) {
      clients[clientCount] = new ServerThread(this, socket);
      try {
        clients[clientCount].open();
        clients[clientCount].start();
        clientCount++;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
