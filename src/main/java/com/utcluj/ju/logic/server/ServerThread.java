package com.utcluj.ju.logic.server;

import com.utcluj.ju.gui.ServerGUI;
import com.utcluj.ju.logic.model.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by Bogdan on 05.11.2015.
 */
class ServerThread extends Thread {

  public SocketServer server = null;
  public Socket socket = null;
  public int ID = -1;
  public String username = "";
  public ObjectInputStream streamIn = null;
  public ObjectOutputStream streamOut = null;

  public ServerThread(SocketServer server, Socket socket) {
    super();
    this.server = server;
    this.socket = socket;
    ID = socket.getPort();
  }

  public void send(Message msg) {
    try {
      streamOut.writeObject(msg);
      streamOut.flush();
    } catch (IOException ex) {
      System.out.println("Exception [SocketClient : send(...)]");
    }
  }

  public int getID() {
    return ID;
  }

  public void run() {
    while (true) {
      try {
        Message msg = (Message) streamIn.readObject();
        server.handle(ID, msg);
      } catch (Exception ioe) {
        System.out.println(ID + " ERROR reading: " + ioe.getMessage());
        server.remove(ID);
        stop();
      }
    }
  }

  public void open() throws IOException {
    streamOut = new ObjectOutputStream(socket.getOutputStream());
    streamOut.flush();
    streamIn = new ObjectInputStream(socket.getInputStream());
  }

  public void close() throws IOException {
    if (socket != null) {
      socket.close();
    }
    if (streamIn != null) {
      streamIn.close();
    }
    if (streamOut != null) {
      streamOut.close();
    }
  }
}
