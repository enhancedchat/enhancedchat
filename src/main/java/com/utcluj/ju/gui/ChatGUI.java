package com.utcluj.ju.gui;

import com.utcluj.ju.logic.model.Message;
import com.utcluj.ju.logic.client.SocketClient;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by Bogdan on 05.11.2015.
 */
public class ChatGUI extends JFrame {

  public JButton jButton1;
  public JButton jButton2;
  public JList jList1;
  public JTextArea jTextArea1;
  public JTextField jTextField1;
  public JTextField jTextField2;
  public SocketClient client;
  public int port;
  public String serverAddr, username;
  public Thread clientThread;

  public DefaultListModel model;

  public ChatGUI() {
    initComponents();
    this.setTitle("Messenger Client");
    jList1.setSelectedIndex(0);

    this.addWindowListener(new WindowListener() {

      public void windowOpened(WindowEvent e) {
      }

      public void windowClosing(WindowEvent e) {
        try {
          client.send(new Message("message", username, ".bye", "SERVER"));
          clientThread.stop();
        } catch (Exception ex) {
        }
      }

      public void windowClosed(WindowEvent e) {
      }

      public void windowIconified(WindowEvent e) {
      }

      public void windowDeiconified(WindowEvent e) {
      }

      public void windowActivated(WindowEvent e) {
      }

      public void windowDeactivated(WindowEvent e) {
      }
    });
  }

  private void initComponents() {
    jTextField1 = new javax.swing.JTextField();
    JLabel jLabel4 = new JLabel();
    JSeparator jSeparator1 = new JSeparator();
    JScrollPane jScrollPane1 = new JScrollPane();
    jTextArea1 = new javax.swing.JTextArea();
    JScrollPane jScrollPane2 = new JScrollPane();
    jList1 = new javax.swing.JList();
    jTextField2 = new javax.swing.JTextField();
    jButton2 = new javax.swing.JButton();
    jButton1 = new javax.swing.JButton();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    jLabel4.setText("Username :");

    jTextArea1.setColumns(20);
    jTextArea1.setFont(new java.awt.Font("Consolas", 0, 12));
    jTextArea1.setRows(5);
    jScrollPane1.setViewportView(jTextArea1);

    jList1.setModel((model = new DefaultListModel()));
    jScrollPane2.setViewportView(jList1);

    jButton2.setText("Send Message ");
    jButton2.setEnabled(false);
    jButton2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton2ActionPerformed(evt);
      }
    });

    jButton1.setText("Login");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton1ActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(
                                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextField1))
                                .addGap(18, 18, 18)
                                .addGroup(
                                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGroup(
                                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING))))
                        .addGroup(
                            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                    layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(
                                            javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                70, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(
                                            javax.swing.GroupLayout.Alignment.LEADING, false)))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 108,
                            javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGap(18, 18, 18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 108,
                            javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10,
                    javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false))
                .addContainerGap())
    );

    pack();
  }

  private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
    username = jTextField1.getText();
    serverAddr = "localhost";
    port = 13000;

    if (!serverAddr.isEmpty()) {
      try {
        client = new SocketClient(this);
        clientThread = new Thread(client);
        clientThread.start();
        client.send(new Message("test", "testUser", "testContent", "SERVER"));
      } catch (Exception ex) {
        jTextArea1.append("[Application > Me] : Server not found\n");
      }
    }
    jTextArea1.append("[Application > Me] : Connected\n");
    if (!username.isEmpty()) {
      client.send(new Message("signup", username, "", "SERVER"));
    }
  }

  private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
    String msg = jTextField2.getText();
    String target = jList1.getSelectedValue().toString();

    if (!msg.isEmpty() && !target.isEmpty()) {
      jTextField2.setText("");
      client.send(new Message("message", username, msg, target));
    }
  }
}
