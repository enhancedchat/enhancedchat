package com.utcluj.ju.gui;

import com.utcluj.ju.logic.server.SocketServer;

import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * Created by Bogdan on 05.11.2015.
 */
public class ServerGUI extends javax.swing.JFrame {

    private JButton jButton1;

    public ServerGUI() {
        initComponents();
    }

    private void initComponents() {
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Messenger Server");

        jButton1.setText("Deploy server");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                    .addComponent(jButton1)));

        pack();
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        new SocketServer();
        jButton1.setEnabled(false);
    }
}
