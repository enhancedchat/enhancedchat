package com.utcluj.ju.gui.login;

/**
 * Created by Bogdan on 25.10.2015.
 *
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.utcluj.ju.gui.sidebar.*;

public class LoginGUI extends JFrame {
    private JTextField userField;
    private JButton loginButton;
    private final static String title = " Login";
    private boolean valid = false;
    private static ArrayList<String> users = new ArrayList<String>();
    private SidebarGUI sidebarGUI = SidebarGUI.getInstance();
    private static String username = "";

    public LoginGUI() {
        super(title);
        setLayout(null);
        this.setSize(400, 250);
        this.setVisible(true);
        init();
    }

    public static String getUsername() {
        return username;
    }

    public void init() {
        this.setLocation(400, 250);
        userField = new JTextField();
        loginButton = new JButton("Login");
        userField.setBounds(100, 30, 160, 35);
        userField.setText("Username");
        loginButton.setBounds(138, 80, 80, 40);

        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                sidebarGUI.removeUser(username);
            }
        });

        add(userField);
        add(loginButton);

        loginButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                if (!userField.equals("") && !users.contains(userField.getText())) {
                    valid = true;
                    JOptionPane.showMessageDialog(null, "Login Accepted");

                } else {
                    valid = false;
                    JOptionPane.showMessageDialog(null, "Login Denied ! Username doesn't exist ! ");
                }

                if (valid) {
                    username = userField.getText();
                    sidebarGUI.listen(username);
                    users.add(username);
                }
            }

        });
    }
}