package com.utcluj.ju.gui.chat;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ChatGUI implements ActionListener {

    private JFrame frame;
    private JTextField textField;
    private JTextArea display;
    private String myUsername;
    private String username; //TODO: use the serverSocket-client API to connect the users

    /**
     * Create the application.
     */
    public ChatGUI(String myUsername, String username) {
        this.myUsername = myUsername;
        this.username = username;
        initialize();
        this.frame.setVisible(true);
    }


    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 589, 389);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("ChatBox");
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        frame.getContentPane().setLayout(gridBagLayout);

        display = new JTextArea();
        display.setAutoscrolls(false);
        display.setCaretColor(Color.RED);
        display.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
        display.setEditable(false);


        GridBagConstraints gbc_display = new GridBagConstraints();
        gbc_display.gridwidth = 10;
        gbc_display.gridheight = 13;
        gbc_display.insets = new Insets(0, 0, 5, 5);
        gbc_display.fill = GridBagConstraints.BOTH;
        gbc_display.gridx = 5;
        gbc_display.gridy = 1;
        frame.getContentPane().add(display, gbc_display);


        textField = new JTextField();
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.gridwidth = 10;
        gbc_textField.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField.insets = new Insets(0, 0, 5, 5);
        gbc_textField.gridx = 5;
        gbc_textField.gridy = 14;
        frame.getContentPane().add(textField, gbc_textField);
        textField.setColumns(10);

        JButton send = new JButton("Send");
        GridBagConstraints gbc_send = new GridBagConstraints();
        gbc_send.insets = new Insets(0, 0, 5, 5);
        gbc_send.gridx = 15;
        gbc_send.gridy = 14;
        frame.getContentPane().add(send, gbc_send);

        textField.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {

            }

            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    processEvent();
                }

            }

            public void keyReleased(KeyEvent e) {

            }
        });
        send.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        processEvent();
    }

    private void processEvent(){
        String message = textField.getText();
        String displayMessage;

        displayMessage = displayMessage(myUsername, message);
        display.append(displayMessage);
        display.setCaretPosition(display.getDocument().getLength());

        textField.setText("");
    }

    private String displayMessage(String username, String message) {
        return username + ": " + message + "\n";
    }
}