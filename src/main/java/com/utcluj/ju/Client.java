package com.utcluj.ju;

import com.utcluj.ju.gui.ChatGUI;

import javax.swing.*;

/**
 * Created by Bogdan on 11.11.2015.
 */
public class Client {
    public static void main(String args[]) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            System.out.println("Look & Feel exception");
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChatGUI().setVisible(true);
            }
        });
    }
}
